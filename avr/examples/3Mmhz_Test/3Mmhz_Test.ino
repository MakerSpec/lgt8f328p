/*   This is a barebones example showing how
 *   to set the internal oscillator to 32mhz
 *   and the difference.
 *   
 *   In order to ensure everything functions 
 *   correctly, make sure under Tools>Clock
 *   you have it set to 16 or 32mhz or else
 *   things like serial, i2c, SPI, and other
 *   functions that rely on accurate timing 
 *   will not work.
 *   
 *   You can change the clock setting to 16mhz
 *   and comment out the two indicated lines below
 *   to easily see the difference in performance.
 *   The two lines to comment are all that is required
 *   besides changing the clock setting to change from
 *   16Mhz to 32Mhz!
 *   
 */

//comment for 16mhz
#include <avr/power.h>

int currentTime = 0;
uint16_t count = 0;

void setup() {
  //comment for 16mhz
  clock_prescale_set(clock_div_1);
  Serial.begin(9600);
  pinMode(13, OUTPUT);
  currentTime = millis();
}

void loop() {
  float n1 = 1.2;
  float n2 = 3.9;
  while(millis() - currentTime < 15000){
    count += 1;
    n1 *= n2;
    n1 /= n2;
  }
  digitalWrite(13, HIGH);
  Serial.print("Score: ");
  Serial.print(count / 4);
  while(count == count){
    //do nothing until reset
  }
  
}
